# DESCRIPTION

This package is the fully revised version of the FES2014 distribution.
This distribution includes the FES2014 tidal prediction software managed on this
page and the [FES2014 tides databases](data/fes2014/README.rst)

# CREDITS

When using FES2014, please mention: *FES2014 was produced by NOVELTIS, LEGOS,
CLS Space Oceanography Division and CNES. It is distributed by AVISO, with
support from CNES (http://www.aviso.altimetry.fr/)*

# HOW TO INSTALL THE SOFTWARE ?

The complete description of the build method is described in the
[INSTALL](INSTALL) file located in the root of this software.

# HOW TO USE THE SOFTWARE ?

This distribution contains a C API to be used to calculate the tide. The
[API](API.rst) documentation describes the functions to be used to
perform the calculation. The directory [examples](examples) contains an
example of using this API.

> It is also possible to use this library using Python. The
> [README](python/README.rst) file in the Python directory contains more
> information.

# BIBLIOGRAPHY

* Lyard F., L. Carrere, M. Cancet, A. Guillot, N. Picot: *FES2014, a new finite
  elements tidal model for global ocean*, in preparation, to be submitted to
  Ocean Dynamics in 2016.

* Carrere L., F. Lyard, M. Cancet, A. Guillot, N. Picot: *FES 2014, a new tidal
  model - Validation results and perspectives for improvements*, presentation to
  ESA Living Planet Conference, Prague 2016.

# CONTACT

[aviso@altimetry.fr](mailto:aviso@altimetry.fr)